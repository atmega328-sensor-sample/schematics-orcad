# schematics orcad

This repository contains the orcad capture 17.2 project for schematics related to the sensors attached to the ATMega328.

Open "ProjectIotSchematics.opj" in Orcad Capture 17.2 to edit the schematic.

The schematic is printed as a pdf in the file "Sensor_ATMega328_Schematic.pdf"